package wpardel;

import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
public class Base64Encoding 
{
	public void Encode(String FileName) throws IOException
	{
		
		long start = System.nanoTime();
		StringBuilder Encoded = new StringBuilder();
		final String Base64Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";		
		Path path = Paths.get(FileName);
	    byte[] b = Files.readAllBytes(path);		
		List<Character> c = new ArrayList<Character>();
		for(int j = 0 ; j<b.length;j+=3)
		{
			int b1 = 0 ;
			int b2  = 0;
			int b3  = 0;
			try
			{				
				if (j > 0 && (j / 3 * 4) % 76 == 0) c.add('\n');
				b1 = b[j] & 0xff;
				b2 = b[j+1] & 0xff;
				b3 = b[j+2] & 0xff;
			}
			catch (Exception e)
			{
				
			}
			
			int Allints = b1<<16 | b2 << 8  | b3;
			int TakenBack1 = (Allints >> 18)&63,TakenBack2 = (Allints >>12)&63,TakenBack3 = (Allints >> 6)&63
					,TakenBack4 = (Allints )&63;
			c.add(new Character(Base64Chars.charAt(TakenBack1)));			
			c.add(new Character(Base64Chars.charAt(TakenBack2)));			
			if(j == b.length - b.length % 3 && b.length % 3 == 1 ){
				//System.out.println("I = " + i + " HIT3");				
			}
			else{
				
				c.add(new Character(Base64Chars.charAt(TakenBack3)));
				//System.out.println("ALL PASS3");
			}			
			if(j == b.length - b.length % 3 &&(b.length % 3 == 1  || b.length % 3 == 2) ){						
			}
			else
			{				
				c.add(new Character(Base64Chars.charAt(TakenBack4)));
				//System.out.println("ALL PASS4");
			}				
		}
		int PadCount =  b.length % 3;
		for( ; PadCount< 3 ; PadCount++)
		{
			c.add('=');
		}
		for(char s : c)
		{
			Encoded.append(s);
		}
		long end = System.nanoTime();
		long dif = end - start;
		PrintWriter pw =new PrintWriter("Zakodowany.bin");
		pw.write(Encoded.toString());		
		pw.close();	
		System.out.println("TIME = " + (dif / 1000000000.0) + " s");
	}
	

}
